import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from fast_api.settings import Settings

# Inicializar configurações
settings = Settings()

DATABASE_URL = os.getenv('DATABASE_URL')

# Criar o engine do SQLAlchemy
engine = create_engine(settings.DATABASE_URL)

# Criar o SessionLocal para ser usado como dependência
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Dependência para obter uma sessão de banco de dados
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def get_session() -> Session:
    """
    Retorna uma nova sessão do banco de dados.
    """
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()