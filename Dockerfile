# Base
FROM python:3.12-slim

# Definir variáveis de ambiente necessárias
ENV POETRY_VIRTUALENVS_CREATE=false

# Definir diretório de trabalho
WORKDIR /app

# Instalar dependências do sistema antes de instalar dependências Python
RUN apt-get update && \
    apt-get install -y gcc python3-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Copiar arquivos do projeto para o contêiner
COPY pyproject.toml poetry.lock /app/
COPY . /app/

# Instalar o Poetry e as dependências do projeto
RUN pip install poetry && \
    poetry config installer.max-workers 10 && \
    poetry install --no-interaction --no-ansi

# Expor a porta para o servidor
EXPOSE 8000

# Comando para iniciar o aplicativo FastAPI com Uvicorn
CMD poetry run uvicorn --host 0.0.0.0 --port 8000 fast_api.app:app